Symbol

Installation
============

This module requires the core CKEditor module.

1. Download the plugin from http://ckeditor.com/addon/symbol.
2. Place the plugin in the root libraries folder (/libraries).
3. Enable Symbol module in the Drupal admin.
4. Configure your CKEditor toolbar to include the button